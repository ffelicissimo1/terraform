resource "aws_internet_gateway" "gtw" {
    vpc_id = aws_vpc.main.id

    tags = {
        Name = "RicNet"
    }
}