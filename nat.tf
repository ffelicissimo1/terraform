resource "aws_eip" "gtw" {
    count = var.az_count
    vpc = true

    tags = {
        Environment = "IaaSWeeK"
    }
}

resource "aws_nat_gateway" "gtw" {
    count           = var.az_count
    subnet_id       = element(aws_subnet.public.*.id, count.index)
    allocation_id   = element(aws_eip.gtw.*.id, count.index)

    tags = {
        Name = "Ricnet"
    }
}